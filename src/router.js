import { createRouter, createWebHistory } from 'vue-router'

const routes = [
    {
        path: '/',
        name: 'home',
        component: () => import('./components/Dishes.vue')
    },

    {
        path: '/add-dish',
        name: 'addDish',
        component: () => import('./components/AddDish.vue')
    },

    {
        path: '/edit-dish/:id',
        name: 'editDish',
        component: () => import('./components/EditDish.vue')
    },

    {
        path: '/view-dish/:id',
        name: 'viewDish',
        component: () => import('./components/ViewDish.vue')
    },
]

const router = createRouter({
    history: createWebHistory(),
    routes,
})

export default router
