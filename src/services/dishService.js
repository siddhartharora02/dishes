import axios from 'axios';

const API_URL = 'https://dishes-be.onrender.com/dishes';
export const getDishes = () => {
    return axios.get(API_URL);
}

export const clearDishes = () => {
    return axios.get(`${API_URL}/clear`);
}

export const getDish = (id) => {
    return axios.get(`${API_URL}/${id}`);
}

export const saveDish = (dish) => {
    return axios.put(`${API_URL}`, dish);
}

export const deleteDish = (id) => {
    return axios.delete(`${API_URL}/${id}`);
}
