const AVAILABILITY = {
    DAYS: [
        {
            label: 'Weekdays',
            value: 'weekdays'
        },
        {
            label: 'Weekends',
            value: 'weekends'
        }
    ],
    TIME: [
        {
            label: 'Breakfast',
            value: 'breakfast'
        },
        {
            label: 'Lunch',
            value: 'lunch'
        },
        {
            label: 'Dinner',
            value: 'dinner'
        }
    ]
}
const CATEGORIES = [
    {
        label: 'Starters',
        value: 'starters'
    },
    {
        label: 'Main Course',
        value: 'main-course'
    },
    {
        label: 'Dessert',
        value: 'dessert'
    },
    {
        label: 'Beverage',
        value: 'beverage'
    }
]

export { AVAILABILITY, CATEGORIES }
