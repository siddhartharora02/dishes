import { CATEGORIES, AVAILABILITY } from '@/utils/constants'

const getCategoryLabel = (category) => {
    const categoryObj = CATEGORIES.find(x => x.value === category)

    return categoryObj ? categoryObj.label : ''
}

const getServingDaysLabel = (dayType) => {
    const dayTypeObj = AVAILABILITY.DAYS.find(x => x.value === dayType)
    return dayTypeObj ? dayTypeObj.label : ''
}

const getMealTimeLabel = (mealTime) => {
    const mealTimeObj = AVAILABILITY.TIME.find(x => x.value === mealTime)
    return mealTimeObj ? mealTimeObj.label : ''
}

export {
    getCategoryLabel,
    getServingDaysLabel,
    getMealTimeLabel
}
