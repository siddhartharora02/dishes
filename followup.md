## Implementation

### 1. What libraries did you add to the frontend? What are they used for?
**Axios**: For making HTTP requests to the backend.

**TailwindCSS**: for styling the frontend.

**Tailwind Forms**: For improving the UI/UX a bit.

### 2. What would you change about your implementation if you had to handle thousands of items?
1. Pagination on the backend and frontend.
2. LIST View as default and an option to switch the view to GRID
3. Search, filter and sort
4. Global state management library (this was a basic app, not a lot of cases to use a "single source of truth")

## General
### 1. If you had more time, what further improvements or new features would you add?
1. Pagination on the frontend only.
2. I could've implemented validation library for the form, but since it was a basic app, I didn't use it.
3. I could've used a global toast messages library for showing success and error messages.
4. I could've used a theme, to set up primary and secondary colors at-least, instead of hard-coding colors like bg-rose-100 etc.
5. Image uploader to upload dish images to make it more visually appealing.
6. LINTING - Forgot to add it, but I would've added it if I had more time.


### 2. Which parts are you most proud of? And why?
The filtering of dishes on the basis of categories, although this was a very small application to get that sense, but otherwise I liked how the final UI looks.

### 3. Which parts did you spend the most time with? What did you find most difficult?
UI/UX: It takes a bit of research before on how it's done usually, also I had limited idea about the design, so that's where the most time went.

### 4. How did you find the test overall? Did you have any issues or have difficulties completing? If you have any suggestions on how we can improve the test, we'd love to hear them.
The test was a good and simple. I had some difficulty deciding whether to put "Breakfast, Lunch and Dinner" in a single array with "Weekdays and weekends", but since the assignment asked for a single property, I decided to extend it. Because these are 2 different types of information.

I also did a bit of hack for clearing dishes. (could've done better)

We could add a little complexity by asking them to add a global store, pagination, file upload (dish image), maybe auth, or using a single screen for CRUD etc. 😛

I did an assignment last year for the current company, that was also an interesting one, you can find it here - https://easypractice.netlify.app/, I would be happy to share the code if you like :) 
